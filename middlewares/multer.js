const multer = require("multer");

const filterImage = (req, file, cb) => {
  if (file.mimetype === 'image/jpg' || file.mimetype === 'image/png') {
      cb(null, true);
  } else {
      cb(null, 'must be upload image format jpg or png');
  }
};

const filterPoster = (req, file, cb) => {
  if (file.mimetype === 'image/jpg' || file.mimetype === 'image/png') {
      cb(null, true);
  } else {
      cb(null, 'must be upload image format jpg or png');
  }
};

const filterTrailer = (req, file, cb) => {
  if (file.mimetype === 'video/mp4' || file.mimetype === 'video/mkv') {
      cb(null, true);
  } else {
      cb(null, 'must be upload image format mp4 or mkv');
  }
};


const storageUser = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './uploads/users');
  },
  filename: (req, file, cb) => {
    cb(null, `${Date.now()}--${file.originalname}`);
  },
});
const storagePoster = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './uploads/posters');
  },
  filename: (req, file, cb) => {
    cb(null, `${Date.now()}--${file.originalname}`);
  },
});
const storageTrailer = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './uploads/trailers');
  },
  filename: (req, file, cb) => {
    cb(null, `${Date.now()}--${file.originalname}`);
  },
});

const uploadUser = multer({storage: storageUser,fileFilter: filterImage});
const uploadPoster = multer({storage: storagePoster,fileFilter: filterPoster});
const uploadTrailer = multer({storage: storageTrailer,fileFilter: filterTrailer});


module.exports = {uploadUser,uploadPoster,uploadTrailer};
