const {Movie,Review,User} = require('../models')
const {tokenVerifier} = require('../helpers/jwt')

const authenticationAdmin = (req, res, next) => {
    console.log(" Authentication Works !");
    
    const {access_token} = req.headers;

    if (!access_token){
        res.status(404).json({
            msg : "Token not found"
        })
    }else {
        try {
            const decode = tokenVerifier(access_token)
            req.adminData = decode
            next();
        }catch (err) {
            res.status(400).json(err);
        }
    }
}
const authorizationAdmin = (req, res, next) => {
    console.log("Authorization Is Works !");
    const id = req.params.id;
    const AdminId =  req.adminData.id


    Movie.findall({
        where: {
            id
        }
    }).then (review=>{
        if(review){
            if(review.AdminId===AdminId){
                next();
            }else{
                throw {
                    status: 403,
                    msg: "Admin doesn't have any acces"
                }
            }
        }else{
            throw {
                status: 404,
                msg : "Movie not Found"
            }
        }
    }).catch(err=>{
        res.status(500).json(err)
    })
    
}


const authentication = (req, res, next) => {
    console.log(" Authentication Works !");
    
    const {access_token} = req.headers;

    if (!access_token){
        res.status(404).json({
            msg : "Token not found"
        })
    }else {
        try {
            const decode = tokenVerifier(access_token)
            req.userData = decode
            next();
        }catch (err) {
            res.status(400).json(err);
        }
    }
}

const authorization = (req, res, next) => {
    console.log("Authorization Is Works !");
    const id = req.params.id;
    const UserId =  req.userData.id


    Review.findone({
        where: {
            id
        }
    }).then (review=>{
        if(review){
            if(review.UserId===UserId){
                next();
            }else{
                throw {
                    status: 403,
                    msg: "User doesn't have any acces"
                }
            }
        }else{
            throw {
                status: 404,
                msg : "Review not Found"
            }
        }
    }).catch(err=>{
        res.status(500).json(err)
    })
    
}

module.exports = {
    authentication,authorization,authenticationAdmin,authenticationAdmin
}