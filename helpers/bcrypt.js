const bcrypt = require('bcrypt');
const saltround = Number(process.env.SALT_ROUND);

const encryptPwd = password => bcrypt.hashSync(password,saltround);
const decryptPwd = (password, userPwd) => bcrypt.compareSync(password,userPwd);

module.exports = {
    encryptPwd, decryptPwd
}