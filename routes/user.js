const { Router } = require('express');
const router = Router();
const UserController = require('../controllers/User')
const {uploadUser} = require('../middlewares/multer')
const { authentication } = require('../middlewares/auth')

router.get('/', UserController.listUser);
router.get('/edit/', authentication, UserController.editFormUser);
router.post('/edit/', uploadUser.single('profileImage'), authentication, UserController.editUser);
router.delete('/', authentication, UserController.deleteUser);


 module.exports = router;


