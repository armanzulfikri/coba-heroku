const { Router } = require('express');
const router = Router();
const MovieController = require('../controllers/Movie')


router.get('/',MovieController.getMovie);
router.get('/search', MovieController.searchmovie);
router.get('/:id', MovieController.detailMovie);
router.post('/add',MovieController.addMovie);
router.delete('/delete/:id', MovieController.deleteMovie);
router.post('/edit/:id', MovieController.editMovie);


 module.exports = router;
