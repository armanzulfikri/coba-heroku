const { Movie} = require ('../models');
const { Op } = require('sequelize');
class MovieController {
    static async getMovie (req, res){
        try {
            const movies= await Movie.findAll({
                order : [
                    ['id','asc']
                ],
                
            });
            res.status (200).json(movies);
        } catch (err) {
            res.status(500).json(err);
        }
    }
    static async detailMovie (req, res){
        const id = req.params.id;
        try {
            const detail = await Movie.findOne({
                where: {id}
            });
            res.status(200).json(detail)
        }catch (err) {
            res.status(500).json(err);
        }
    }
    static async searchmovie (req, res) {
        const { search } = req.body;
        try {
            const found = await Movie.findAll({
                where: {
                    title: {
                        [Op.like]: '%' + search + '%'
                    }
                }
            })
            if(found){
                res.status(201).json(found);
            } else {
                res.status(409).json({
                    msg: "Movie is not available!"
                });
            }
        }catch (err) {
            res.status(500).json(err);
        }
    }
    static async filterGenre (req, res) {
        const {genre} = req.params;
        try{
            const filter =  await Movie.findAll({
                where: {genre}
            })
            res.status(200).json(filter);
        }catch (err) {
            status(500).json(err);
        }

    }
    static async addMovie (req, res){
        const{title,director,genre,synopsis,cast,rate,poster,trailer,realeased_date} =  req.body;
        
        try {
                const movie = await Movie.create({
                    title,
                    director,
                    genre,
                    synopsis,
                    cast,
                    rate,
                    poster,
                    trailer,
                    realeased_date
                    
                })
                res.status(201).json(movie);
            
        }catch (err) {
            res.status(500).json(err);
        }
    }

    static async editMovie (req, res){
        const id = req.params.id;
        const{title,director,genre,synopsis,cast,rate,poster,trailer,realeased_date} =  req.body;
        try{
            const movie = await Movie.update({
                title,
                director,
                genre,
                synopsis,
                cast,
                rate,
                poster,
                trailer,
                realeased_date
            },{
                where: { id }
            });
            res.status(201).json({
                movie,
                msg: "Movie edited successfully!"
            }); 
        } catch (err) {
            res.status(500).json(err)
        }

    }
    static async deleteMovie (req, res) {
        const id =req.params.id;
        
        try{
            const hapus = await Movie.destroy({
                where: {
                    id
                }
                
            })
            res.status(200).json({
                hapus,
                msg : "Movie Deleted"
            });
        }catch(err){
            res.status(500).json(err);
        }
    }

}

module.exports = MovieController;
