const { User } = require ('../models');
const {decryptPwd} = require('../helpers/bcrypt')
const {tokenGenerator} = require('../helpers/jwt')


class UserController {
    static async listUser (req, res){
        try {
            const users = await User.findAll({
                order : [
                    ['name','asc']
                ]
            });
            res.status (200).json(users);
        } catch (err) {
            res.status(500).json(err);
        }
    }

    static async register(req, res) {
        const { nama, email, password } = req.body;
        const profileImage = req.file.path;
        try {
            const check = await User.findOne({
                where: { email }
            });
            if (check) {
                res.status(409).json("Email has been registered Try another Email.");
            } else {
                const user = await User.create({
                    nama,
                    email,
                    password,
                    profileImage
                });
                res.status(201).json( user );
            }
        } catch (err) {
            res.status(500).json(err);
        }
    }

    static async login(req, res) {
        const { email, password } = req.body;
        try {
            const user = await User.findOne({
                where: { email }
            });
            if (user) {
                if (decryptPwd(password, user.password)) {
                    const access_token = tokenGenerator(user)
                    res.status(200).json({ access_token });
                } else {
                    res.status(400).json("Password incorrect!")
                }
            } else {
                res.status(404).json("User not found!");
            }
        } catch (err) {
            res.status(500).json(err)
        }
    }
    static async editUser(req, res) {
        const id = req.userData.id;
        const { name } = req.body;
        const profileImage = req.file.path;
        console.log(req.file)

        try {
            const update = await User.update({
                name,
                profileImage
            }, {
                where: { id }
            });
            res.status(203).json(update);
        } catch (err) {
            res.status(500).json(err);
        }
    }
    static async editFormUser(req, res) {
        const id = req.params.id;
        User.findOne({
            where : { id }
        })
        .then(result => {
            res.status(200).json(result)
        })
        .catch(err=>{
            res.status(500).json(err)
        })
    }


    static async deleteUser(req, res) {
        const id = req.userData.id

        try {
            const result = await User.destroy({
                where: { id }
            });
            res.status(202).json(result);
        } catch (err) {
            res.status(500).json(err);
        }
    }
}





module.exports = UserController;




