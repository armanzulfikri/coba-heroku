# Movie app
creating movie app is an application to manage your assets. This app has : RESTful endpoint for e-comerce-cms app CRUD operation

link web
#

RESTful endpoint

POST Register

Request Header 
not needed 

Request Body
Response (200)[
  {
    "id": 1,
    "nama": "<asset name>",
    "email": "<asset email>",
    "password": "<asset password>",
    "profileImage": "<asset Image>"(use multer),
    "createdAt": "2020-03-20T07:15:12.149Z",
    "updatedAt": "2020-03-20T07:15:12.149Z",
  }
]

Response (500 - Bad Request){
  "message": "<returned error message>"
}
POST Login

Request Header 
not needed 

Request Body
Response (200)[
  {
    "accestoken": "<asset accestoken>",
  }
]

Response (500 - Bad Request){
  "message": "<returned error message>"
}
------------------------------------------
Api Movie

Post addMovie

Request Header 
not needed 

Request Body
Response (200)[
  {
    "id": 1,
    "title": "<asset title>",
    "director": "<asset director>",
    "sinopsis": "<asset sinopsis>",
    "cast": "<asset cast>",
    "poster" : "<asset image>" (use.multer),
    "trailer" : "<asset url>"
    "realeasedDate" : "<asset date>"
    "createdAt": "2020-03-20T07:15:12.149Z",
    "updatedAt": "2020-03-20T07:15:12.149Z",
  }
]

Response (500 - Bad Request){
  "message": "<returned error message>"
}


Get /Movie

Request Header 
not needed 

Request Body
Response (200)[
  {
    "id": 1,
    "title": "<asset title>",
    "director": "<asset director>",
    "sinopsis": "<asset sinopsis>",
    "cast": "<asset cast>",
    "poster" : "<asset image>" (use.multer),
    "trailer" : "<asset url>"
    "realeasedDate" : "<asset date>"
    "createdAt": "2020-03-20T07:15:12.149Z",
    "updatedAt": "2020-03-20T07:15:12.149Z",
  }
]

Response (500 - Bad Request){
  "message": "<returned error message>"
}
Api Movie

Get /Movie/1

get movie sesuai id  (detail movie)

Request Header 
not needed 

Request Body
Response (200)[
  {
    "id": 1,
    "title": "<asset title>",
    "director": "<asset director>",
    "sinopsis": "<asset sinopsis>",
    "cast": "<asset cast>",
    "poster" : "<asset image>" (use.multer),
    "trailer" : "<asset url>"
    "realeasedDate" : "<asset date>"
    "createdAt": "2020-03-20T07:15:12.149Z",
    "updatedAt": "2020-03-20T07:15:12.149Z",
  }
]

Response (500 - Bad Request){
  "message": "<returned error message>"
}